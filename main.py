import json
import random
import sys
import time
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

with open('drivers.conf.json', mode='r') as f:
    conf = json.load(f)

test_url = 'http://localhost:8000/?path=/story/custom-layout--proposed-keyboard'


def test(test_function):
    if 'default' not in conf:
        print('Provide a \"default\" key in `drivers.conf.json`')
        sys.exit(1)

    browser_name = conf['default']

    if browser_name not in conf:
        print(f'Provided browser name `{browser_name}` not registered in `drivers.conf.json`.')
        sys.exit(1)

    service = Service(executable_path=conf[browser_name])

    if browser_name == 'firefox':
        driver = webdriver.Firefox(service=service)
    elif browser_name in ['chromium', 'chrome']:
        driver = webdriver.Chrome(service=service)
    else:
        print(f'Unknown browser type \"{browser_name}\".')
        sys.exit(1)

    test_function(driver)

    # close current tab
    driver.close()

    # close entire browser
    driver.quit()


def send_string_visual(string, target_input, pause_time):
    for ch in string:
        target_input.send_keys(ch)
        time.sleep(pause_time)


class TestVisualKeyboardTest(unittest.TestCase):
    def test_switch_layout_and_write(self):
        def test_function(driver):
            driver.get(test_url)

            driver.switch_to.frame(driver.find_element(by=By.ID, value='storybook-preview-iframe'))

            target_input = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'targetInput')))

            target_input.click()
            time.sleep(1)

            target_input.send_keys(Keys.ALT, Keys.CONTROL)

            time.sleep(1)

            action = ActionChains(driver)
            action.key_down(Keys.SHIFT).pause(.5).send_keys('q').key_up(Keys.SHIFT).perform()

            time.sleep(2)

            send_string_visual('k ', target_input, 0.2)
            send_string_visual('sprawu?', target_input, 1)

            target_input_value = target_input.get_attribute('value')
            expected = 'Як справи?'

            self.assertEqual(target_input_value, expected)

        test(test_function)

    def test_selection_deletion(self):
        def test_function(driver, main_action):
            driver.get(test_url)
            driver.switch_to.frame(driver.find_element(by=By.ID, value='storybook-preview-iframe'))

            target_input = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'targetInput')))
            target_input.click()
            time.sleep(1)

            for ch in 'abcdefghi':
                target_input.send_keys(ch)

            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)

            action = ActionChains(driver)

            time.sleep(1)
            action\
                .key_down(Keys.SHIFT) \
                .send_keys(Keys.ARROW_RIGHT) \
                .send_keys(Keys.ARROW_RIGHT) \
                .send_keys(Keys.ARROW_RIGHT) \
                .key_up(Keys.SHIFT) \
                .perform()
            time.sleep(1)

            target_input.send_keys(main_action)

            time.sleep(2)

            for ch in 'aaa':
                target_input.send_keys(ch)

            target_input_value = target_input.get_attribute('value')
            expected = 'abcaaaghi'

            self.assertEqual(target_input_value, expected)

        # deletion of selection must work for backspace
        test(lambda driver: test_function(driver, main_action=Keys.BACKSPACE))
        # deletion of selection must work for delete as well
        test(lambda driver: test_function(driver, main_action=Keys.DELETE))

    def test_arrows(self):
        def test_function(driver):
            driver.get(test_url)
            driver.switch_to.frame(driver.find_element(by=By.ID, value='storybook-preview-iframe'))

            target_input = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'targetInput')))
            target_input.click()
            time.sleep(1)

            for ch in 'abcdefghi':
                target_input.send_keys(ch)

            time.sleep(2)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            target_input.send_keys(Keys.ARROW_LEFT)
            time.sleep(2)
            target_input.send_keys(Keys.BACKSPACE)
            time.sleep(1)

            expected = 'abcdeghi'

            target_input_value = target_input.get_attribute('value')
            self.assertEqual(target_input_value, expected)

        test(test_function)

    def test_arrow_and_delete(self):
        def test_function(driver):
            driver.get(test_url)
            driver.switch_to.frame(driver.find_element(by=By.ID, value='storybook-preview-iframe'))

            target_input = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'targetInput')))
            target_input.click()
            time.sleep(1)

            for ch in 'vdi':
                target_input.send_keys(ch)

            time.sleep(2)
            target_input.send_keys(Keys.BACKSPACE)
            time.sleep(2)
            target_input.send_keys(Keys.ARROW_LEFT)
            time.sleep(2)
            target_input.send_keys('j')
            time.sleep(2)
            target_input.send_keys('e')
            time.sleep(2)

            expected = 'vjed'

            target_input_value = target_input.get_attribute('value')
            self.assertEqual(target_input_value, expected)

        test(test_function)

    def test_random_writing_and_deleting(self):
        def test_function(driver):
            driver.get(test_url)
            driver.switch_to.frame(driver.find_element(by=By.ID, value='storybook-preview-iframe'))

            target_input = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'targetInput')))
            target_input.click()
            time.sleep(1)

            last_action = []

            written = []
            caret_position = 0
            for _ in range(500):
                if random.randint(1, 10) > 3:
                    random_letter = random.choice(
                        [chr(ch) for ch in range(ord('a'), ord('z') + 1)] + [' ', '.', ',', '?', '!']
                    )

                    target_input.send_keys(random_letter)
                    # print(f'writing \"{random_letter}\"')
                    written.insert(caret_position, random_letter)
                    caret_position += 1

                    last_action.append(f'write \"{random_letter}\"')
                else:
                    # probability of 2/3 we move the caret left or right
                    if random.randint(1, 3) > 1:
                        if random.randint(1, 2) == 1:
                            target_input.send_keys(Keys.ARROW_LEFT)
                            # print(f'arrow left: before {caret_position}')
                            # time.sleep(2)
                            caret_position = max(0, caret_position - 1)
                            # print(f'arrow left: after {caret_position}')
                            # time.sleep(2)
                            last_action.append(f'move caret left')
                        else:
                            # print(f'arrow right: before {caret_position}')
                            # time.sleep(2)
                            target_input.send_keys(Keys.ARROW_RIGHT)
                            caret_position = min(len(written), caret_position + 1)
                            # print(f'arrow right: after {caret_position}')
                            # time.sleep(2)
                            last_action.append(f'move caret right')

                    # probability of 1/3 we delete character
                    else:
                        target_input.send_keys(Keys.BACKSPACE)
                        # print(f'deleting: before {"".join(written)}', '  idx:', caret_position)
                        written = written[:caret_position - 1] + written[caret_position:]
                        caret_position = max(0, caret_position - 1)
                        # print(f'deleting: after {"".join(written)}', '  idx:', caret_position)
                        # time.sleep(.2)
                        last_action.append(f'backspace')

                # print(f'result: \"{"".join(written)}\"')
                if "".join(written) != target_input.get_attribute('value'):
                    print('ERROR: \"' + "".join(written) + '\"', 'should be', '\"' + target_input.get_attribute('value') + '\"', caret_position)
                    time.sleep(30)
                    print(f'Last three actions were \"{last_action[-3:]}\".')
                #     time.sleep(10)
                    # time.sleep(1)

                # time.sleep(1)

            target_input_value = target_input.get_attribute('value')
            expected = ''.join(written)

            self.assertEqual(target_input_value, expected)

        test(test_function)


if __name__ == '__main__':
    unittest.main()
