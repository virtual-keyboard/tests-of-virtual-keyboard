# Installation

Download the drivers of browsers you are going to test on. For example
- Firefox <https://github.com/mozilla/geckodriver/releases> and
- Chromium <https://chromedriver.chromium.org/>.

Then follow the following steps.
```bash
git clone git@gitlab.com:virtual-keyboard/tests-of-virtual-keyboard.git
cd tests-of-virtual-keyboard/

# create virtual environment and install dependencies
python3 -m venv venv/
source venv/bin/activate
pip install -r requirements.txt

# Fill in the drivers' paths for each browser you will test on in "drivers.conf.json". The "default" key contains the browser's name to run the tests on.

# add new tests, e.g., to `main.py`

# to run all the tests in `main.py`
python3 main.py
```

